# Copyright (c) 2020 Xabier Bello (xbello@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os
import strformat

import bio/fasta

type
  Variant* = tuple[start, stop: int, kind: char, altBase, refBase: string]

proc makeVariant*(refBase, altBase: char, pos: int): Variant =
  ## Create a Variant object given the bases involved and the position
  const base: set[char] = {'A', 'C', 'G', 'T'}

  case altBase
  of '-':
    result = (pos, pos, 'D', $altBase, $refBase)
  of 'N':
    result = (pos, pos, 'N', $altBase, $refBase)
  of base:
    if refBase in base:
      result = (pos, pos, 'M', $altBase, $refBase)
    elif refBase == '-':
      result = (pos, pos, 'I', $altBase, $refBase)
  else:
    result = (pos, pos, 'U', $altBase, $refBase)

proc `$`(v: Variant): string =
  case v.kind
  of 'D':
    result = &"{v.start}_{v.stop}del"
  of 'N', 'I':
    result = &"{v.start}_{v.stop}ins{v.altBase}"
  of 'M':
    result= &"{v.start}{v.refBase}>{v.altBase}"
  else:
    result = &"{v.start}_{v.stop}UNDEF"

proc compact(variants: seq[Variant]): seq[Variant] =
  ## Compact a seq of Variants joining adjencent-same-kind Variants into one.
  ## E.g.
  ## 1A>T and 2C>G can be joined into 1AC>TG
  ##
  if variants.len == 0:
    return
  result = @[variants[0]]
  for variant in variants[1 .. ^1]:
    if (variant.kind == result[^1].kind and
        variant.start <= result[^1].stop + 1):
      case variant.kind
      of 'D':
        result[^1].stop = variant.start
      of 'N':
        result[^1].stop = variant.start
        result[^1].altBase.add variant.altBase
      of 'I':
        result[^1].stop = variant.start
        result[^1].altBase.add variant.altBase
      of 'M':
        result[^1].stop = variant.start
        result[^1].altBase.add variant.altBase
        result[^1].refBase.add variant.refBase
      else:
        result.add variant
    else:
      result.add variant

proc main(inFile: string) =
  var refer: SequenceRecord
  var seqVariants: seq[Variant]

  var refPos: int
  for sequence in sequences(inFile):
    if refer == nil:
      refer = sequence
      continue
    seqVariants = @[]
    refPos = 0
    for i, refBase in refer.pairs:
      if refBase != '-':
        refPos.inc
      if refBase != sequence[i]:
        seqVariants.add makeVariant(refBase, sequence[i], refPos)

    stdout.write sequence.name, ";"
    for variant in seqVariants.compact:
      stdout.write variant, ";"
    stdout.write "\n"

when isMainModule:
  ## Search the FASTA file in the input for variants. Reference is the first.
  main(commandLineParams()[0])
