# Copyright (c) 2020 Xabier Bello (xbello@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Classify the variants in groups of impact:
## High:
##  - Transcript deletion
##  - Stop gained / lost
##  - Frameshift
##  - Start loss
##
## Medium:
##  - Missense
##
## Low:
##  - Final codon change
##  - Synonymous, including stop and start codons.
##
## Variable:
##  - Non-coding variant (intergenic)
##
import os
import strformat
import tables

import bio/fasta
from bio/data import codonTable

import searchVariants

type
  Grade = enum
    low, medium, high

  Mutation = ref object of RootObj
    refer, alt: char
    grade: Grade
    desc: string

const orfs: seq[tuple[start, stop: int]] =
  @[(266, 13468), (13468, 21555), (21563, 25384), (25393, 26220),
    (26245, 26472), (26523, 27191), (27202, 27387), (27394, 27759),
    (27894, 28259), (28274, 29533), (29558, 29674)]

proc inOrfs(pos: int): seq[int] =
  ## Return the orf(s) where the mutation is included
  for i, orf in orfs.pairs:
    if pos >= orf.start and pos <= orf.stop:
      result.add i

proc consequence(mut: var Mutation) =
  ## Side-effect Mutation changing the grade and description.
  ##
  if mut.refer == mut.alt:
    mut.grade = Grade.low
    mut.desc = "Synonymous"
  else:
    mut.grade = Grade.medium
    mut.desc = "Missense"
    if mut.alt == '*':
      mut.grade = Grade.high
      mut.desc = "Stop gain"
    if mut.refer == '*':
      mut.grade = Grade.high
      mut.desc = "Stop loss"

proc specialConsequences(mut: var Mutation, offSet, orfNCodons: int) =
  ## Reconsider if the Mutation has special consequences
  if offSet < 3: # First codon
    if mut.refer == 'M' and mut.desc == "Missense":
      mut.grade = Grade.high
      mut.desc = "Start loss"
  if (offSet + 1) div 3 == orfNCodons - 2:
    # We are in the codon just before the last
    if mut.desc == "Missense":
      mut.grade = Grade.low
      mut.desc = "Missense in next to last codon"

proc codonList(dna: SequenceRecord): seq[string] =
  for i in 0  .. (dna.len div 3) - 1:
    result.add dna[(i * 3) .. ((i * 3) + 2)].record.chain

proc mutate(codons: seq[string], pos: int, newBase: char): Mutation =
  ## Given a seq of codons (representing a DNA chain splitted in codons),
  ##  mutate the position pos to newBase and return a tuple with the original
  ##  and new aminoacids.
  ## Position `pos` is 1-indexed (the first base is 1, not 0)
  var codon = codons[(pos - 1) div 3]
  let refer = codonTable[codon]
  codon[(pos - 1) mod 3] = newBase
  let alt = codonTable[codon]

  Mutation(refer: refer, alt: alt)

iterator pointMutations(align: string): Variant =
  var refer: SequenceRecord

  var refPos: int
  var variant: Variant
  for sequence in sequences(align):
    if refer == nil:
      refer = sequence
      continue
    refPos = 0
    for i, refBase in refer.pairs:
      if refBase != '-':
        refPos.inc
      if refBase != sequence[i]:
        variant = makeVariant(refBase, sequence[i], refPos)
        if variant.kind == 'M':
          yield variant

proc main(inFile: string) =
  # Where Reference.fasta is the MN908947.3 sequence from Genbank
  let refer: SequenceRecord = load("Reference.fasta")[0]
  # Extract the ORFs from the Reference sequence
  var orfSeq: seq[SequenceRecord]
  for orf in orfs:
    orfSeq.add refer[orf.start - 1 .. orf.stop - 1]

  var mutations: seq[Variant]
  var variantFrequencies = initCountTable[Variant]()
  for mutation in pointMutations(inFile):
    variantFrequencies.inc(mutation)
    if mutation notin mutations:
      mutations.add mutation

  var mut: Mutation
  var foundIn: seq[int]
  for mutation in mutations:
    foundIn = inOrfs(mutation.start)
    if foundIn.len > 0:
      for idx in foundIn:
        mut = mutate(codonList(orfSeq[idx]),
                     mutation.start - orfs[idx].start + 1, mutation.altBase[0])
        mut.consequence()
        mut.specialConsequences(mutation.start - orfs[idx].start,
                                len(orfSeq[idx].codonList))
        stdout.write &"{mutation.refBase}{mutation.start}{mutation.altBase};"
        stdout.write &"{mut.refer}>{mut.alt};{mut.grade};{mut.desc};"
        stdout.write &"{variantFrequencies[mutation]}\n"
    else:
      # Is not found in any ORF, then is Intergenic
      stdout.write &"{mutation.refBase}{mutation.start}{mutation.altBase};"
      stdout.write "None;Low;Intergenic;"
      stdout.write &"{variantFrequencies[mutation]}\n"

when isMainModule:
  main(commandLineParams()[0])
