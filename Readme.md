# Mapping genome variation of SARS-CoV-2 worldwide highlights the impact of COVID-19 super-spreaders

_Alberto Gómez-Carballa, Xabier Bello, Jacobo Pardo-Seco, Federico Martinón-Torres and Antonio Salas_

> The human pathogen severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) is responsible for the major pandemic of the 21st century. We analyzed >4,700 SARS-CoV-2 genomes and associated meta-data retrieved from public repositories. SARS-CoV-2 sequences have a high sequence identity (>99.9%), which drops to >96% when compared to bat coronavirus genome. We built a mutation-annotated reference SARS-CoV-2 phylogeny with two main macro-haplogroups, A and B, both of Asian origin, and >160 sub-branches representing virus strains of variable geographical origins worldwide, revealing a rather uniform mutation occurrence along branches that could have implications for diagnostics and the design of future vaccines. Identification of the root of SARS-CoV-2 genomes is not without problems, owing to conflicting interpretations derived from either using the bat coronavirus genomes as an outgroup or relying on the sampling chronology of the SARS-CoV-2 genomes and TMRCA estimates; however, the overall scenario favors haplogroup A as the ancestral node. Phylogenetic analysis indicates a TMRCA for SARS-CoV-2 genomes dating to 12 November 2019 - thus matching epidemiological records. Sub-haplogroup A2 most likely originated in Europe from an Asian ancestor and gave rise to sub-clade A2a, which represents the major non-Asian outbreak, especially in Africa and Europe. Multiple founder effect episodes, most likely associated with super-spreader hosts, might explain COVID-19 pandemic to a large extent. 

## Supplementary code

This is the companion of scripts for the publication:

[https://doi.org/10.1101/gr.266221.120](https://doi.org/10.1101/gr.266221.120)

Also previously pre-printed at:

https://www.biorxiv.org/content/10.1101/2020.05.19.097410v1

There are mainly simple scripts, importing a more complex library
[bio](https://gitlab.com/xbello/bio). Almost all of them are intended to use
with stdout redirect, like:

    $ command fastaFile.fas > outputFastaFile.fas

Programs are focused in getting the data as raw as possible from the alignments,
so it can be later analyzed with R or Excel.

> This is **not** a tutorial nor a helping page about Nim coding or compiling.
For more info about the Nim programming language, visit:
[Nim lang](https://nim-lang.org/)

> This is **not** a tutorial about R coding.

> This is **not** a tutorial on how to use [bio](https://gitlab.com/xbello/bio).

For more info about our group and interests:

<img src="genvip.png" width="50"> [GenVip](http://genvip.eu/)

<img src="logo.png" width="50"> [GenPob](https://www.genpob.eu/)

[NanostrinGenvip](https://nanostringenvip.com/en/)

## removeRepeated.nim

Prints sequences from a FASTA that were not previously seen, thus removing
sequences that are identical and keeping only one of them.

## searchVariants.nim

Search for variants in an alignment. Also provides useful procedures for other
scripts.

Used for Supplementary table S1, and indirectly for S2, S3, S6, S8

## variantsConsequences.nim

Find the variants in the alignment, calculates the aminoacid change and
classify them.

Used for Supplementary tables S2, S3 and specially S7.

## Diversity index script.R

To compute diversity index (nucleotide diversity, haplotype diversity and
Tajima's index).

Used in Figures S8, S10 and Table S3.